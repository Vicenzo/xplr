------ catppuccin colorscheme
local colorscheme = {
	red = { 243, 139, 168 },
	maroon = { 235, 160, 172 },
	flamingo = { 242, 205, 205 },
	mauve = { 203, 166, 247 },
	peach = { 250, 179, 135 },
	yellow = { 249, 226, 175 },
	green = { 166, 227, 161 },
	teal = { 148, 226, 213 },
	blue = { 137, 180, 250 },
	lavender = { 180, 190, 254 },
	base = { 30, 30, 46 },
	sapphire = { 116, 199, 236 },
	black = { 17, 17, 27 },
	overlay = { 108, 112, 134 },
	mantle = { 24, 24, 37 },
	rosewater = { 245, 224, 220 },
	text = { 205, 214, 244 },
	sky = { 137, 220, 235 },
}
for color, rgb in pairs(colorscheme) do
	_G[color] = { Rgb = rgb }
end
