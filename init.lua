---
version = "0.21.3"
local home = os.getenv("HOME") .. "/.config/xplr/" -- source_path
local xpm_path = home .. "xpm/?.lua;"
local plugin_path = home .. "plugins/?/init.lua;"
local home_path = home .. "?.lua;"
--- plugins auto_install
local plugins = {
	"Junker/nuke.xplr",
	"dtomvan/ouch.xplr",
	"sayanarijit/qrcp.xplr",
	"emsquid/style.xplr",
}
--- file-sources-path
package.path = package.path .. ";" .. xpm_path .. plugin_path .. home_path
--- files_and_plugins-setup
require("xpm").setup(plugins)
require("qrcp").setup({
	mode = "action",
	key = "Q",
})
require("nuke").setup({
	pager = "more",
	open = {
		run_executables = true,
		custom = {
			{ mime_regex = "^video/.*", command = "vlc {}" },
			{ mime_regex = ".*", command = "sudo lvim {}" },
		},
	},
	view = {},
	smart_view = {
		custom = {
			{ extension = "so", command = "ldd -r {} | less" },
		},
	},
})
require("ouch").setup({
	mode = "action",
	key = "o",
})
--- source files
require("colors")
require("layout")
require("icons")
require("keys")
require("ui")
require("log")
------- logs text
return {
	on_load = {
		{ LogSuccess = "loaded!" },
	},
	on_directory_change = {
		{ LogWarning = "" },
	},
	on_focus_change = {
		{ LogInfo = "󰹺" },
	},
}
