local cols = {}
local S = require("style")
local style = S.style
local parse_style = S.parse_style
------- colums_display_by_index
for i = 0, 4 do
	table.insert(cols, { format = "builtin.fmt_general_table_row_cols_" .. i })
end
------ 🌐 General
local general = {
	enable_mouse = false,
	show_hidden = false,
	read_only = false,
	------ Default UI
	default_ui = { prefix = "", suffix = "" },
	panel_ui = {
		default = {
			title = { format = "𐌞", style = { bg = red, fg = black } },
			borders = { "Bottom", "Top", "Left", "Right" },
			border_type = "Thick",
			border_style = { bg = base, fg = lavender },
		},
		input_and_logs = {
			title = { format = "" },
			style = { bg = base, fg = base },
			borders = { "Bottom", "Left", "Right", "Top" },
			border_type = "Rounded",
			border_style = { bg = base, fg = overlay },
		},
	},
	------ table-tree
	table = {
		header = {
			height = 1,
			style = {
				bg = { Rgb = { 100, 63, 255 } },
			},
		},
		tree = { {}, {}, {} },
		row = {
			cols = cols,
		},
		col_spacing = 0,
		col_widths = {
			{ Percentage = 10 },
			{ Percentage = 30 },
			{ Percentage = 20 },
			{ Percentage = 15 },
			{ Percentage = 25 },
		},
	},
	------ focus and selection states
	focus_ui = {
		prefix = "  ",
		suffix = " ",
		style = { fg = red, bg = base },
	},
	selection_ui = {
		prefix = "  ",
		suffix = "",
		style = { fg = overlay, bg = mantle, add_modifiers = { "Bold" } },
	},

	focus_selection_ui = {
		prefix = "  ",
		suffix = "",
		style = { fg = red, bg = mantle, add_modifiers = { "Bold" } },
	},
}
------- keys
for key, val in pairs(general) do
	xplr.config.general[key] = val
end
--------
local s = parse_style({ bg = "DarkGray", add_modifiers = { "Bold" } })
xplr.fn.builtin.fmt_general_table_row_cols_4 = function(m)
	return tostring(os.date(s("%b  %d 󰅒 %H:%M"), m.last_modified / 1000000000))
end
xplr.fn.builtin.fmt_general_table_row_cols_3 = function(m)
	if not m.is_dir then
		return style.fg[teal]("" .. m.human_size)
	else
		return style.fg[lavender]("")
	end
end
