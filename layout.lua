local style = require("style").style
local function c(s, rgb)
	return style.fg[rgb](s)
end

local function bit(s, rgb, cond)
	if cond then
		return c(s, rgb)
	else
		return c(overlay, "-")
	end
end

local function datetime(num)
	return tostring(os.date("%a %b %d %H:%M:%S %Y", num / 1000000000))
end

local function permissions(p)
	local r = ""

	r = r .. bit("r", green, p.user_read)
	r = r .. bit("w", yellow, p.user_write)

	if p.user_execute == false and p.setuid == false then
		r = r .. bit("-", red, p.user_execute)
	elseif p.user_execute == true and p.setuid == false then
		r = r .. bit("x", red, p.user_execute)
	elseif p.user_execute == false and p.setuid == true then
		r = r .. bit("S", red, p.user_execute)
	else
		r = r .. bit("s", red, p.user_execute)
	end

	r = r .. bit("r", green, p.group_read)
	r = r .. bit("w", yellow, p.group_write)

	if p.group_execute == false and p.setuid == false then
		r = r .. bit("-", red, p.group_execute)
	elseif p.group_execute == true and p.setuid == false then
		r = r .. bit("x", red, p.group_execute)
	elseif p.group_execute == false and p.setuid == true then
		r = r .. bit("S", red, p.group_execute)
	else
		r = r .. bit("s", red, p.group_execute)
	end

	r = r .. bit("r", green, p.other_read)
	r = r .. bit("w", yellow, p.other_write)

	if p.other_execute == false and p.setuid == false then
		r = r .. bit("-", red, p.other_execute)
	elseif p.other_execute == true and p.setuid == false then
		r = r .. bit("x", red, p.other_execute)
	elseif p.other_execute == false and p.setuid == true then
		r = r .. bit("T", red, p.other_execute)
	else
		r = r .. bit("t", red, p.other_execute)
	end

	return r
end

local function stat(node)
	local type = node.mime_essence
	if node.is_symlink then
		if node.is_broken then
			type = " "
		else
			type = " " .. node.symlink.absolute_path
		end
	end

	return style.fg[lavender]("Type     : ")
		.. type
		.. style.fg[yellow]("\nSize     : ")
		.. node.human_size
		.. style.fg[flamingo]("\nOwner    : ")
		.. string.format("%s:%s", node.uid, node.gid)
		.. style.fg[text]("\nPerm     : ")
		.. permissions(node.permissions)
		.. style.fg[mauve]("\nCreated  : ")
		.. datetime(node.created)
		.. style.fg[red]("\nModified : ")
		.. datetime(node.last_modified)
end

local function read(path, height)
	local p = io.open(path)

	if p == nil then
		return nil
	end

	local i = 0
	local res = ""
	for line in p:lines() do
		if line:match("[^ -~\n\t]") then
			p:close()
			return
		end

		res = res .. line .. "\n"
		if i == height then
			break
		end
		i = i + 1
	end
	p:close()

	return res
end

local function offset(listing, height)
	local h = height - 3
	local start = (listing.focus - (listing.focus % h))
	local result = {}
	for i = start + 1, start + h, 1 do
		table.insert(result, listing.files[i])
	end
	return result, start
end

local function list(parent, focused, explorer_config)
	local files, focus = {}, 0
	local config = { sorters = explorer_config.sorters }
	local ok, nodes = pcall(xplr.util.explore, parent, config)
	if not ok then
		nodes = {}
	end

	for i, node in ipairs(nodes) do
		local rel = node.relative_path
		if rel == focused then
			focus = i
		end
		if node.is_dir then
			rel = rel .. "/"
		end
		table.insert(files, rel)
	end

	return { files = files, focus = focus }
end

local function tree_view(listing, height)
	local count = #listing.files
	local files, start = offset(listing, height)
	local res = {}
	for i, file in ipairs(files) do
		table.insert(res, style.fg[overlay](" ") .. style.fg[mauve](file))
	end
	return res
end

local function render_preview(ctx)
	local n = ctx.app.focused_node

	if n then
		if n.is_file then
			local success, res = pcall(read, n.absolute_path, ctx.layout_size.height)
			if success and res ~= nil then
				return res
			else
				return stat(n)
			end
		elseif n.is_dir then
			local listing = list(n.absolute_path, nil, ctx.app.explorer_config)
			return table.concat(tree_view(listing, ctx.layout_size.height), "\n")
		else
			return stat(n)
		end
	else
		return ""
	end
end

local preview = {
	CustomContent = {
		body = {
			DynamicParagraph = {
				render = "custom.render_preview",
			},
		},
	},
}

xplr.fn.custom.render_preview = render_preview
local layout = {
	Horizontal = {
		config = {
			constraints = {
				{ Percentage = 65 },
				{ Percentage = 35 },
			},
		},
		splits = {
			"Table",
			preview,
		},
	},
}
local full_layout = {
	Vertical = {
		config = {
			constraints = {
				{ Min = 1 },
				{ Length = 3 },
			},
		},
		splits = {
			layout,
			"InputAndLogs",
		},
	},
}
xplr.config.layouts.builtin.default = full_layout
