xplr.config.general.logs = {
	success = {
		format = "",
		style = { fg = green, add_modifiers = { "Bold", "Italic" } },
	},
	warning = {
		format = "",
		style = { fg = teal, add_modifiers = { "Italic" } },
	},
	error = {
		format = "",
		style = { bg = red, fg = black, add_modifiers = { "Bold", "Italic" } },
	},
	info = { style = { fg = mauve, add_modifiers = { "Italic" } } },
}
